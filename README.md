# demo-test

## Requirements 

Install the following packages with pip:

```
pip install google-cloud-aiplatform
```

Set the ADC (Application Default Credentials) to authenticate against GCP: 
```
gcloud auth application-default login
```
Credentials will be stored in the local environment in `/Users/<USER>/.config/gcloud/application_default_credentials.json'`
