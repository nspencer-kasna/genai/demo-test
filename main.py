from google.cloud import aiplatform
from vertexai.language_models import TextGenerationModel

PROJECT_ID = "<PROJECT_ID>"
STAGING_BUCKET = "<STAGING_BUCKET>"

aiplatform.init(
    project=PROJECT_ID,
    location='us-central1',
    staging_bucket=STAGING_BUCKET,
    experiment='my-experiment',
    experiment_description='my experiment description'
)

generation_model = TextGenerationModel.from_pretrained("text-bison@001")

# Here is where the question is asked
prompt = "Make a list for the best language for data science?"

temperature = 0.2
top_p_val = 0.8
top_k_val = 40
token_limit = 256

response = generation_model.predict(
    prompt=prompt,
    temperature=temperature,
    top_p=top_p_val,
    top_k=top_k_val,
    max_output_tokens=token_limit
)

print(response.text)